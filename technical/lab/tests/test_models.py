from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.utils.http import urlencode
from ..models.personaFisica import PersonaFisica
from ..models.personaJuridica import PersonaJuridica
from ..models.titulares import Titulares
from ..api.resources import PersonaFisicaViewSet
from ..api.resources import PersonaJuridicaViewSet
import json


class TestApi(APITestCase):
    URL_PERSONA_FISICA = '/personafisica/'
    URL_PERSONA_JURIDICA = '/personajuridica/'




    def setUp(self) -> None:
        # garantiza por las dudas que no hay registros guardados
        assert PersonaFisica.objects.count() == 0
        assert PersonaJuridica.objects.count() == 0
        assert Titulares.objects.count() == 0




    def agregar_persona_fisica(self, data):
        # wrapper de metodo post de persona fisica
        return self.client.post(self.URL_PERSONA_FISICA, data)




    def agregar_persona_juridica(self, data):
        # wrapper de metodo post de persona juridica
        return self.client.post(self.URL_PERSONA_JURIDICA, data)






    def borrar_persona_fisica(self, cuit):
        return self.client.delete(self.URL_PERSONA_FISICA + str(cuit) + '/')




    def borrar_persona_juridica(self, cuit):
        return self.client.delete(self.URL_PERSONA_JURIDICA + str(cuit) + '/')




    def modificacion_persona_juridica(self, datos):
        return self.client.patch(self.URL_PERSONA_JURIDICA + str(datos['cuit']) + '/', datos)




    def test_url_inexistente(self):
        request = self.client.get('/asdf/')
        assert request.status_code == status.HTTP_404_NOT_FOUND




    def obtener_resultados(self, request):
        # convierte
        jsonResponse = json.loads(request.content)
        return jsonResponse['results']




    def test_persona_fisica(self):
        data = {"cuit": 20, "nombre": "ivancito", "apellido": "paredes", "dni": 313213231}
        self.agregar_persona_fisica(data)
        request = self.client.get(self.URL_PERSONA_FISICA)
        assert request.status_code == status.HTTP_200_OK

        assert self.obtener_resultados(request)[0] == data
        # garantiza que lo insertado coincide con lo enviado
        assert PersonaFisica.objects.get().cuit == data['cuit']
        assert PersonaFisica.objects.get().nombre == data['nombre']
        assert PersonaFisica.objects.get().apellido == data['apellido']
        assert PersonaFisica.objects.get().dni == data['dni']




    def test_persona_fisica_especifica(self):
        data = {"cuit": 20, "nombre": "ivancito", "apellido": "paredes", "dni": 313213231}
        self.agregar_persona_fisica(data)

        request = self.client.get(self.URL_PERSONA_FISICA, data)
        assert request.status_code == status.HTTP_200_OK

        assert self.obtener_resultados(request)[0] == data
        # garantiza que lo insertado coincide con lo enviado
        assert PersonaFisica.objects.get().cuit == data['cuit']
        assert PersonaFisica.objects.get().nombre == data['nombre']
        assert PersonaFisica.objects.get().apellido == data['apellido']
        assert PersonaFisica.objects.get().dni == data['dni']




    def test_persona_juridica_especifica(self):
        data = {"cuit": 20, "razon_social": "tenaris", "anio_fundacion": 2010}
        self.agregar_persona_juridica(data)

        request = self.client.get(self.URL_PERSONA_JURIDICA, data)
        assert request.status_code == status.HTTP_200_OK

        assert self.obtener_resultados(request)[0] == data
        # garantiza que lo insertado coincide con lo enviado
        assert PersonaJuridica.objects.get().cuit == data['cuit']
        assert PersonaJuridica.objects.get().razon_social == data['razon_social']
        assert PersonaJuridica.objects.get().anio_fundacion == data['anio_fundacion']




    def test_persona_juridica(self):
        data = {"cuit": 20, "razon_social": "tenaris", "anio_fundacion": 2010}
        self.agregar_persona_juridica(data)
        request = self.client.get(self.URL_PERSONA_JURIDICA)

        assert request.status_code == status.HTTP_200_OK

        # garantiza que coincida el response con lo enviado en la request
        assert self.obtener_resultados(request)[0] == data

        # garantiza que lo insertado coincide con lo enviado
        assert PersonaJuridica.objects.get().cuit == data['cuit']
        assert PersonaJuridica.objects.get().razon_social == data['razon_social']
        assert PersonaJuridica.objects.get().anio_fundacion == data['anio_fundacion']




    def test_insertado_persona_fisica(self):
        data = {"cuit": 20, "nombre": "ivancito", "apellido": "paredes", "dni": 313213231}
        response = self.agregar_persona_fisica(data)
        assert response.status_code == status.HTTP_201_CREATED
        assert PersonaFisica.objects.count() == 1
        assert Titulares.objects.count() == 1




    def test_insertado_fallido_persona_fisica(self):
        # envia menos datos de los obligatorios
        data = {"cuit": 20, "apellido": "paredes", "dni": 313213231}
        response = self.agregar_persona_fisica(data)
        print(response)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert PersonaFisica.objects.count() == 0
        assert Titulares.objects.count() == 0




    def test_insertado_fallido_persona_fisica_datos_invalidos(self):
        # envia menos datos de los obligatorios
        data = {"cuit": 20, "apellido": "paredes", "nombre": "ivancito", "dni": 1}
        response = self.agregar_persona_fisica(data)
        print(response)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert PersonaFisica.objects.count() == 0
        assert Titulares.objects.count() == 0




    def test_insertado_mismo_cuit(self):
        data = {"cuit": 20, "nombre": "ivancito", "apellido": "paredes", "dni": 313213231}
        self.agregar_persona_fisica(data)
        data = {"cuit": 20, "nombre": "jose", "apellido": "avila", "dni": 3192788}
        response = self.agregar_persona_fisica(data)

        # verifica que no se pudo insertar la segunda persona fisica, y que los datos guardados son de la primera
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert Titulares.objects.count() == 1
        assert PersonaFisica.objects.count() == 1
        assert PersonaFisica.objects.get().cuit == 20
        assert PersonaFisica.objects.get().nombre == 'ivancito'
        assert PersonaFisica.objects.get().apellido == 'paredes'
        assert PersonaFisica.objects.get().dni == 313213231




    def test_insertado_mismo_cuit_distinto_titular(self):
        data = {"cuit": 20, "nombre": "ivan", "apellido": "paredes", "dni": 313213231}
        self.agregar_persona_fisica(data)
        data = {"cuit": 20, "razon_social": "juju sa", "anio_fundacion": 2010}
        response = self.agregar_persona_juridica(data)

        # verifica que no se pudo insertar la segunda persona juridica, y que los datos guardados son de la primera
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert PersonaJuridica.objects.count() == 0
        assert PersonaFisica.objects.count() == 1
        assert Titulares.objects.count() == 1




    def test_insertado_distinto_cuit(self):
        data = {"cuit": 21, "nombre": "jose", "apellido": "avila", "dni": 3192788}
        self.agregar_persona_fisica(data)
        data2 = {"cuit": 20, "nombre": "ivancito", "apellido": "paredes", "dni": 313213231}
        response = self.agregar_persona_fisica(data2)

        print("data:", response.data)
        print("atributos disponibles y sus respuestas:", response.__dict__)

        # verifica que se pudo insertar la segunda persona fisica correctamente
        assert response.status_code == status.HTTP_201_CREATED

        # verifica que la cantidad de registros insertados son 2
        assert PersonaFisica.objects.count() == 2
        assert Titulares.objects.count() == 2

        # verifica que el registro de la bd con el segundo cuit ingresado coincide con el pasado previamente
        query = PersonaFisica.objects.get(cuit=data2['cuit'])
        assert query.cuit == data2['cuit']
        assert query.nombre == data2['nombre']
        assert query.apellido == data2['apellido']
        assert query.dni == data2['dni']




    def test_borrado_persona_fisica(self):
        data = {"cuit": 21, "nombre": "jose", "apellido": "avila", "dni": 3192788}
        self.agregar_persona_fisica(data)

        # verifica que se inserta correctamente
        assert PersonaFisica.objects.count() == 1
        response = self.borrar_persona_fisica(data['cuit'])

        # verifica que se borro correctamente
        assert PersonaFisica.objects.count() == 0
        assert Titulares.objects.count() == 0
        assert response.status_code == status.HTTP_204_NO_CONTENT




    def test_borrado_persona_fisica_inexistente(self):
        data = {"cuit": 1567498}
        response = self.borrar_persona_fisica(data['cuit'])
        assert response.status_code == status.HTTP_404_NOT_FOUND




    def test_borrado_persona_juridica_inexistente(self):
        data = {"cuit": 1567498}
        response = self.borrar_persona_juridica(data['cuit'])
        assert response.status_code == status.HTTP_404_NOT_FOUND




    def test_borrado_persona_juridica(self):
        data = {"cuit": 21, "razon_social": "tenaris", "anio_fundacion": 2000}
        self.agregar_persona_juridica(data)

        # verifica que se inserta correctamente
        assert PersonaJuridica.objects.count() == 1
        response = self.borrar_persona_juridica(data['cuit'])

        assert response.status_code == status.HTTP_204_NO_CONTENT
        # verifica que se borro correctamente
        assert PersonaJuridica.objects.count() == 0
        assert Titulares.objects.count() == 0




    def test_modificacion_persona_juridica(self):
        data = {"cuit": 21, "razon_social": "tenaris", "anio_fundacion": 2000}
        self.agregar_persona_juridica(data)

        # verifica que se inserta correctamente
        assert PersonaJuridica.objects.count() == 1

        data2 = {"cuit": 21, "razon_social": "ypf"}
        response = self.modificacion_persona_juridica(data2)

        # verifica que se modifico correctamente
        assert PersonaJuridica.objects.count() == 1
        assert Titulares.objects.count() == 1
        assert PersonaJuridica.objects.get().razon_social == data2['razon_social']
        assert response.status_code == status.HTTP_200_OK






    def test_borrado_persona_juridica_anio_invalido(self):
        data = {"cuit": 21, "razon_social": "tenaris", "anio_fundacion": 2030}
        response = self.agregar_persona_juridica(data)
        # verifica que se inserta correctamente
        assert PersonaJuridica.objects.count() == 0
        assert response.status_code == status.HTTP_400_BAD_REQUEST
