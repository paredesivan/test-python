from datetime import datetime
from django.core.validators import MinLengthValidator, MaxValueValidator
from django.db import models
from .titulares import Titulares


class PersonaJuridica(Titulares):
    # hereda de titulares. mas info en
    # https://docs.djangoproject.com/en/3.0/topics/db/models/#multi-table-inheritance
    class Meta:
        ordering = ('razon_social',)

    # todos los atributos son obligatorios por defecto
    razon_social = models.CharField(verbose_name="razón social", max_length=100, validators=[MinLengthValidator(2)])

    # valida que el año sea igual o menor al actual
    anio_fundacion = models.PositiveSmallIntegerField(verbose_name="año de fundación",
                                                      validators=[MaxValueValidator(datetime.now().year)])
