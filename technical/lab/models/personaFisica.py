from django.core.validators import MinValueValidator, MinLengthValidator
from django.db import models
from .titulares import Titulares


class PersonaFisica(Titulares):
    # clase persona fisica hereda de titulares
    class Meta:
        ordering = ('nombre',)

    # todos los atributos son obligatorios
    nombre = models.CharField(verbose_name="nombre", max_length=80, validators=[MinLengthValidator(2)])
    apellido = models.CharField(verbose_name="apellido", max_length=250, validators=[MinLengthValidator(2)])
    dni = models.PositiveIntegerField(verbose_name="DNI", validators=[MinValueValidator(999999)])
