from django.db import models


class Titulares(models.Model):
    # el tipo de especializacion es disyuntiva total: debe ser de uno y solo uno de los dos subtipos (fisica y juridica)
    cuit = models.PositiveIntegerField(verbose_name="CUIT", primary_key=True)

