from rest_framework import serializers
from ..models.personaFisica import PersonaFisica
from ..models.personaJuridica import PersonaJuridica



class PersonaFisicaSerializer(serializers.ModelSerializer):
    class Meta:
        # modelo a serializar
        model = PersonaFisica

        # atributos a serializar del modelo, en este caso todos
        fields = "__all__"



class PersonaJuridicaSerializer(serializers.ModelSerializer):
    class Meta:
        # modelo a serializar
        model = PersonaJuridica

        # atributos a serializar del modelo, en este caso todos
        fields = "__all__"

