from rest_framework import viewsets
from .serializers import PersonaFisicaSerializer
from .serializers import PersonaJuridicaSerializer
from ..models.personaFisica import PersonaFisica
from ..models.personaJuridica import PersonaJuridica
from .responses import Borrado


# modelviewset es un viewset que provee por defecto `create()`, `retrieve()`, `update()`,
# `partial_update()`, `destroy()` and `list()` actions. pueden ser redefinidas tranquilamente
class TitularesViewSet(viewsets.ModelViewSet):

    # redefino este metodo invocado al querer borrar un registro a modo de prueba. ambas subclases lo heredaran
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        print("borrado correcto")
        return Borrado(data={'mensaje': 'borrado exitoso'})


class PersonaFisicaViewSet(TitularesViewSet):
    serializer_class = PersonaFisicaSerializer
    queryset = PersonaFisica.objects.all().order_by("cuit")



class PersonaJuridicaViewSet(TitularesViewSet):
    serializer_class = PersonaJuridicaSerializer
    queryset = PersonaJuridica.objects.all().order_by("cuit")
