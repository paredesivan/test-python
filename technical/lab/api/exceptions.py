from rest_framework import status
from werkzeug.exceptions import BadRequest
from django.utils.translation import ugettext_lazy as _

# clase que se puede implementar a la hora de lanzar excepciones
class Forbidden(BaseException):
    default_detail = _("No tenes permisos")
    status_code = status.HTTP_403_FORBIDDEN
