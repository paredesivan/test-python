from django import http
from rest_framework.response import Response
from rest_framework import status


class OK(Response):
    status_code = 200


class Error(Response):
    status_code = 400


class Creado(Response):
    status_code = 201


class Aceptado(Response):
    status_code = 202


class Prohibido(Response):
    status_code = 403


class Borrado(Response):
    status_code = 204


class RecursoNoDisponible(Response):
    status_code = 404
