from django.contrib import admin
from .models.personaFisica import PersonaFisica
from .models.personaJuridica import PersonaJuridica


class PersonaFisicaAdmin(admin.ModelAdmin):
    list_per_page = 10
    # campos de personas fisicas que se veran en el panel de administracion
    list_display = ("cuit", "nombre", "apellido", "dni")  # mostrara esos campos
    search_fields = ("cuit", "nombre", "apellido", "dni")  # agrega para buscar por cualquiera de los campos


class PersonaJuridicaAdmin(admin.ModelAdmin):
    # campos de personas juridicas que se veran en el panel de administracion
    list_per_page = 10
    list_filter = ("anio_fundacion",)
    search_fields = ("cuit", "razon_social", "anio_fundacion")
    list_display = (
        "cuit", "razon_social", "anio_fundacion")  # me lo muestra como una tabla
    # entonces podre filtrar


# ya con esto permite administrar personas desde el admin y le indico que campos mostrare
admin.site.register(PersonaFisica, PersonaFisicaAdmin)

# ya con esto permite administrar personas
admin.site.register(PersonaJuridica, PersonaJuridicaAdmin)
