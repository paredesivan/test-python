"""technical URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from .lab.api.resources import PersonaFisicaViewSet
from .lab.api.resources import PersonaJuridicaViewSet

router = routers.DefaultRouter()

# crea los listados automaticamente
router.register(prefix=r"personafisica", viewset=PersonaFisicaViewSet)
router.register(prefix=r"personajuridica", viewset=PersonaJuridicaViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(arg=router.urls)),
    path('api-auth/', include(arg='rest_framework.urls', namespace='rest_framework'))
]
