1. Agregar un titular de tipo persona física
    ~~~
    curl -X POST -H "Content-Type: application/json; Accept: application/json; indent=4" -u admin:admin http://localhost:8000/personafisica/ -d "{\"cuit\": \"123456\", \"nombre\":\"ivan\",\"apellido\":\"paredes\",\"dni\":31631228}"
    ~~~

2. Agregar un titular de tipo persona jurídica
    ~~~
    curl -X POST -d "{\"cuit\":12341232, \"razon_social\":\"tenaris\", \"anio_fundacion\":2010}" -H "Content-Type:application/json;Accept: application/json; indent=4" http://127.0.0.1:8000/personajuridica/ -u admin:admin 
    ~~~
   
3. listar todos los titulares de tipo persona fisica
    ~~~
    curl -H 'Accept: application/json; indent=4' -u admin:admin http://localhost:8000/personafisica/
    ~~~
   
4. Obtener titular especifico de tipo persona fisica. siendo 123456 el cuit del titular
    ~~~
    curl -H 'Accept: application/json; indent=4' -u admin:admin http://localhost:8000/personafisica/123456/
    ~~~
   
5. listar todos los titulares de tipo persona juridica
    ~~~
    curl -H 'Accept: application/json; indent=4' -u admin:admin http://localhost:8000/personajuridica/
    ~~~
   
6. Obtener titular especifico de tipo persona juridica. siendo 12341232 el cuit del titular
    ~~~
    curl -H 'Accept: application/json; indent=4' -u admin:admin http://localhost:8000/personajuridica/12341232/
    ~~~
   
7. Modificar un dato de un titular de tipo de persona física con cuit:123456
    ~~~
    curl -X PATCH -H "Content-Type: application/json; Accept: application/json; indent=4" -u admin:admin http://localhost:8000/personafisica/123456/ -d "{\"nombre\": \"raul\"}"
    ~~~
   
8. Modificar un dato de un titular de tipo de persona juridica con cuit:12341232
    ~~~
    curl -X PATCH -H "Content-Type: application/json; Accept: application/json; indent=4" -u admin:admin http://localhost:8000/personajuridica/12341232/ -d "{\"razon_social\": \"flinstones\"}"
    ~~~

9. modificar todos los datos de una persona fisica con cuit 123456
    ~~~
    curl -X PUT -H "Content-Type: application/json; Accept: application/json; indent=4" -u admin:admin http://localhost:8000/personafisica/123456/ -d "{\"cuit\": 123456, \"nombre\": \"raul\",\"apellido\":\"menendez\", \"dni\": 66666666}"
    ~~~
    
10. modificar todos los datos de una persona juridica con cuit 12341232
    ~~~
    curl -X PUT -H "Content-Type: application/json; Accept: application/json; indent=4" -u admin:admin http://localhost:8000/personajuridica/12341232/ -d "{\"cuit\":12341232,\"razon_social\": \"flintstones\",\"anio_fundacion\": 2015}"
    ~~~
    
11. Borrar una persona fisica con cuit 123456
    ~~~
    curl -X DELETE -H 'Accept: application/json; indent=4' -u admin:admin http://localhost:8000/personafisica/123456/
    ~~~
   
12. Borrar una persona juridica con cuit 12341232
    ~~~
    curl -X DELETE -H 'Accept: application/json; indent=4' -u admin:admin http://localhost:8000/personajuridica/12341232/
    ~~~
   

